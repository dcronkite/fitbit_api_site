from distutils.core import setup
import setuptools
import os

with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'README.md'),
          encoding='utf-8') as f:
    long_description = f.read()

setup(name='fitbit_api_site',
      version='0.0.2',
      description='Manages data from multiple Fitbit accounts',
      long_description=long_description,
      url='https://bitbucket.org/dcronkite/fitbit_api_site',
      author='dcronkite',
      author_email='dcronkite@gmail.com',
      license='MIT',
      classifiers=[  # from https://pypi.python.org/pypi?%3Aaction=list_classifiers
          'Development Status :: 1 - Planning',
          'Intended Audience :: Science/Research',
          'Programming Language :: Python :: 3 :: Only',
      ],
      keywords='fitbit',
      entry_points={
          'console_scripts':
              [
                  'start-fitbit-site = fitbit_api_site:main',
                  'create-fitbit-db = fitbit_api_site.db:main_create',
                  'manage-fitbit-db = fitbit_api_site.db:main'
              ]
      },
      install_requires=[
          'wtforms',
          'apscheduler',
          'pytz',
          'python-dateutil',
          'fitbit',
          'oauthlib',
          'flask',
          'requests',
          'flask-alembic',
          'flask_migrate',
          'flask_script',
          'flask_security',
          'flask_wtf',
          'flask_restless',
          'flask-apscheduler',
          'cherrypy',
          'paste',
          'bcrypt',
      ],
      extras_require={
          'mysql': ['mysqlclient', 'mysql-connector']
      },
      package_dir={'': 'src'},
      package_data={
          '': ['static/*'],
          'static': ['*.txt']
      },
      include_package_data=True,
      packages=setuptools.find_packages('src'),
      zip_safe=False
      )
