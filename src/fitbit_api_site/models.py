from datetime import datetime

from flask_security import UserMixin, RoleMixin
from fitbit_api_site import db

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(30), unique=True)
    password = db.Column(db.String(255))
    real_name = db.Column(db.String(80))
    call_name = db.Column(db.String(20))
    active = db.Column(db.Boolean, default=1)
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))


class FitbitUser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    display_name = db.Column(db.String(100))  # fitbit displayName
    username = db.Column(db.String(30), unique=True)  # internal tracking
    user_id = db.Column(db.String(100), unique=True)  # fitbit encodedId
    date_added = db.Column(db.DateTime)
    access_token = db.Column(db.String(500))
    refresh_token = db.Column(db.String(200))
    last_refresh = db.Column(db.DateTime)
    token_expires_seconds = db.Column(db.Integer)

    def __init__(self, **kwargs):  # display_name, username, user_id, access_token, refresh_token, token_scope_seconds):
        super(FitbitUser, self).__init__(**kwargs)
        self.date_added = datetime.utcnow()
        self.last_refresh = datetime.utcnow()

    def update_tokens(self, access_token, refresh_token):
        self.last_refresh = datetime.utcnow()
        self.access_token = access_token
        self.refresh_token = refresh_token


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(50))


class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    entry = db.Column(db.String(50))


class FitbitEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fitbit_user = db.Column(db.Integer, db.ForeignKey('fitbit_user.id'))
    date_added = db.Column(db.DateTime)
    event_date = db.Column(db.DateTime)
    category = db.Column(db.Integer, db.ForeignKey('category.id'))
    entry = db.Column(db.Integer, db.ForeignKey('entry.id'))
    value = db.Column(db.String(50))
    active = db.Column(db.Boolean)

    def __init__(self, fitbit_user, event_date, category, entry, value):
        self.value = value
        self.entry = entry
        self.category = category
        self.fitbit_user = fitbit_user
        self.event_date = event_date
        self.active = 1
        self.date_added = datetime.now()
