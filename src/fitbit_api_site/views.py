import csv
import os
import random
import string
from datetime import datetime
import sqlalchemy as sqla

from flask import jsonify
from flask import request, url_for, redirect
from flask import render_template
from flask import send_file
from flask_security import login_required, roles_required, roles_accepted, Security, SQLAlchemyUserDatastore
from flask_security.utils import hash_password
from flask_restless import APIManager

from fitbit_api_site import app, db
from fitbit_api_site import models
from fitbit_api_site.fitbit import MyFitbit

api_manager = APIManager(app, flask_sqlalchemy_db=db)
user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
security = Security(app, user_datastore)


@app.before_first_request
def create_users_and_roles():
    db.create_all()

    # check and create roles
    admin_role = user_datastore.find_or_create_role(name="admin", description="Full site administrator")
    download_role = user_datastore.find_or_create_role(name="download", description="Has download privileges")

    # check & add users
    user1 = user_datastore.find_user(email="admin@example.com")
    user2 = user_datastore.find_user(email="download@example.com")
    user3 = user_datastore.find_user(email="user@example.com")

    if not user1:
        user1 = user_datastore.create_user(email='admin@example.com', password=hash_password('password123'))
        user_datastore.add_role_to_user(user1, admin_role)

    if not user2:
        user2 = user_datastore.create_user(email='download@example.com', password=hash_password('password456'))
        user_datastore.add_role_to_user(user2, download_role)

    if not user3:
        user_datastore.create_user(email='user@example.com', password=hash_password('password789'))

    db.session.commit()


EXPIRES_IN = 31536000
FITBIT = None  # todo: move to session


def config(key, default=None):
    try:
        return app.config[key]
    except KeyError:
        return default


@app.route('/client/', methods=['GET'])
# @permission_required(models.Permission.ADMIN)
@roles_required("admin")
def view_client():
    return 'Client ID: {}; Client Secret: {}'.format(config('CLIENT_ID'), config('CLIENT_SECRET'))


@app.route('/callback', methods=['GET'])
@login_required
def view_keys():
    global FITBIT
    error_messages = []
    FITBIT.client.fetch_access_token(request.args['code'], config('CALLBACK'))
    fitbit_user = FITBIT.user_profile_get()
    try:
        fitbit_user = fitbit_user['user']
    except KeyError:
        app.logger.error('Failed to load user.')
        app.logger.exception(str(fitbit_user))

    fbu_exists_q = models.FitbitUser.query.filter_by(user_id=fitbit_user['encodedId'])
    if fbu_exists_q.count() > 0:
        user = fbu_exists_q.first()
        user.access_token = FITBIT.get_access_token()
        user.refresh_token = FITBIT.get_refresh_token()
        user.token_expires_in = EXPIRES_IN
        db.session.commit()
        error_messages.append(
            'The Fitbit user id already exists. Ensure that this fitbit user has not yet been added to the database.')
        return redirect(url_for('index'))
    else:
        return render_template('create_user.html',
                               display_name=fitbit_user['displayName'],
                               user_id=fitbit_user['encodedId'],
                               access_token=FITBIT.get_access_token(),
                               refresh_token=FITBIT.get_refresh_token(),
                               )


@app.route('/user/create', methods=['POST'])
@login_required
def create_user():
    error_messages = []

    if models.FitbitUser.query.filter_by(username=request.json['username']).count() > 0:
        error_messages.append(
            'That username already exists.')
        return render_template('create_user.html',
                               display_name=request.json['display_name'],
                               username=request.json['username'],
                               user_id=request.json['user_id'],
                               access_token=request.json['access_token'],
                               refresh_token=request.json['refresh_token']
                               )

    user = models.FitbitUser(display_name=request.json['display_name'],
                             username=request.json['username'],
                             user_id=request.json['user_id'],
                             access_token=request.json['access_token'],
                             refresh_token=request.json['refresh_token'],
                             token_expires_seconds=EXPIRES_IN
                             )

    try:
        db.session.add(user)
        db.session.commit()
    except sqla.exc.IntegrityError as e:
        error_messages.append('Failed to add user.')
        error_messages.append(str(e))

    for msg in error_messages:
        app.logger.error(msg)
    return redirect(url_for('index'))


@app.route('/', methods=['GET'])
@login_required
def index():
    return render_template('index.html')


@app.route('/user/add/', methods=['POST'])
@login_required
def get_token():
    if config('CLIENT_ID') and config('CLIENT_SECRET'):
        global FITBIT
        FITBIT = MyFitbit(config('CLIENT_ID'), config('CLIENT_SECRET'))
        url, _ = FITBIT.client.authorize_token_url(redirect_uri=config('CALLBACK'),
                                                   expires_in=EXPIRES_IN,
                                                   prompt='login'  # always require user to login
                                                   )
        return redirect(url)
    return redirect(url_for('index'))


@app.route('/user/list/', methods=['GET'])
@login_required
def get_users():
    users = models.FitbitUser.query.all()
    res = {'data': [{
                        'username': user.username,
                        'display_name': user.display_name
                    }
                    for user in users]}
    return jsonify(res)


@app.route('/refresh/activities/', methods=['GET'])
@login_required
def get_refresh_activities():
    """Get activties for all active fitbit users and update backend data"""
    return jsonify({'data': refresh_activities()})


@app.route('/refresh/activities/count/', methods=['GET'])
@login_required
def get_refresh_activities_count():
    """Get activties for all active fitbit users and update backend data"""
    results, error_messages = refresh_activities()
    error_messages.append('Updated ' + str(len(results)) + ' records.')
    return jsonify({'count': len(results),
                    'error_messages': error_messages})


def refresh_activities():
    users = models.FitbitUser.query.all()
    res = []
    error_messages = []
    for user in users:
        global FITBIT
        FITBIT = MyFitbit(config('CLIENT_ID'), config('CLIENT_SECRET'),
                          access_token=user.access_token,
                          refresh_token=user.refresh_token)
        try:
            FITBIT.user_profile_get(user.user_id)
        except Exception as e:
            error_messages.append(
                'Failed to update user: "{}" due to "{}".'.format(user.display_name, e.args[0].split(':')[0]))
            continue
        res.append(user.id)
        args = [user.user_id, config('START_DATE'), config('END_DATE')]
        make_fitbit_api_request(user.id, 'activities-tracker-calories',
                                FITBIT.get_tracker_calories(*args))
        make_fitbit_api_request(user.id, 'activities-tracker-steps',
                                FITBIT.get_tracker_steps(*args))
        make_fitbit_api_request(user.id, 'activities-tracker-distance',
                                FITBIT.get_tracker_distance(*args))
        make_fitbit_api_request(user.id, 'sleep', FITBIT.get_sleep_range(*args))

        # update access tokens
        user.update_tokens(FITBIT.get_access_token(), FITBIT.get_refresh_token())
        db.session.commit()
    return res, error_messages


KEYS = {
    'activities-tracker-calories': [('ACTIVITY', 'CALORIES', ['value'])],
    'activities-tracker-steps': [('ACTIVITY', 'STEPS', ['value'])],
    'activities-tracker-distance': [('ACTIVITY', 'DISTANCE', ['value'])],
    'sleep': [
        ('SLEEP', 'TOTAL_MINUTES', ['summary', 'totalMinutesAsleep']),
        ('SLEEP', 'TOTAL_RECORDS', ['summary', 'totalSleepRecords']),
        ('SLEEP', 'TOTAL_IN_BED', ['summary', 'totalTimeInBed']),
    ]
}


def _get_value(row, value_list):
    if len(value_list) == 0:
        return str(row)
    else:
        if value_list[0] in row:
            return _get_value(row[value_list[0]], value_list[1:])
        else:
            return None


def make_fitbit_api_request(fitbit_user_id, key, result):
    for row in result[key]:
        for category, entry, value_key in KEYS[key]:
            add_fitbit_item(fitbit_user_id, row['dateTime'], category, entry, _get_value(row, value_key))


def add_fitbit_item(fitbit_user_id, event_date, category, entry, value):
    # format event date if still a datetime
    if isinstance(event_date, datetime):
        event_date = '{}-{:02d}-{:02d}'.format(event_date.year, event_date.month, event_date.day)

    # check to see if category exists
    category_q = models.Category.query.filter_by(category=category)
    if category_q.count() == 0:
        category_item = models.Category(category=category)
        db.session.add(category_item)
        db.session.commit()
    else:
        category_item = category_q.first()
    # check to see if entry exists
    entry_q = models.Entry.query.filter_by(entry=entry)
    if entry_q.count() == 0:
        entry_item = models.Entry(entry=entry)
        db.session.add(entry_item)
        db.session.commit()
    else:
        entry_item = entry_q.first()

    # check to see if user has same event_date, category, entry and de-activate
    fbe_exists_q = models.FitbitEntry.query.filter_by(
        fitbit_user=fitbit_user_id,
        event_date=event_date,
        category=category_item.id,
        entry=entry_item.id,
        active=1
    )
    if fbe_exists_q.count() > 0:
        for fbe in fbe_exists_q.all():
            fbe.active = 0

    # insert new entry
    fbe = models.FitbitEntry(
        fitbit_user=fitbit_user_id,
        event_date=event_date,
        category=category_item.id,
        entry=entry_item.id,
        value=value
    )
    db.session.add(fbe)

    # commit changes
    db.session.commit()


@app.route('/download/data/', methods=['GET'])
@roles_accepted('download', 'admin')  # @permission_required(models.Permission.DOWNLOAD)
def download_current_data():
    data = db.session.query(
        models.FitbitEntry.event_date,
        models.FitbitUser.username,
        models.Category.category,
        models.Entry.entry,
        models.FitbitEntry.value
    ).join(
        models.FitbitUser, models.FitbitUser.id == models.FitbitEntry.fitbit_user
    ).join(
        models.Category, models.Category.id == models.FitbitEntry.category
    ).join(
        models.Entry, models.Entry.id == models.FitbitEntry.entry
    ).filter(
        models.FitbitEntry.active == 1
    ).order_by(
        models.FitbitUser.username,
        models.FitbitEntry.event_date
    ).all()
    doc_name = 'HEALTH_TEAMING_DATA_{}.CSV'.format(datetime.today().strftime('%Y%m%d'))
    doc_path = os.path.join(config('OUT_DIR'),
                            ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(30)))
    with open(doc_path, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['SUBJECT', 'EVENT_DATE', 'CATEGORY', 'ENTRY', 'VALUE'])
        for event_date, username, category, entry, value in data:
            writer.writerow([username, event_date, category, entry, value])
    return send_file(doc_path, as_attachment=True, attachment_filename=doc_name)
