/**
 * Created by David on 9/27/2016.
 */
// (function(angular) {
    'use strict';   // See note about 'use strict'; below

    var myApp = angular.module('FitbitAPI', []);

    myApp.controller('GetFitbitUsers', function ($scope, $http) {
        $http.get('/user/list/')
            .then(
                function (data) {
                $scope.users = data.data['data'];
                console.log(data.data);
            },
            function (error) {
                console.log(error);
            });
    });

    myApp.controller('FormController', function($scope, $http) {
        $scope.refresh = function() {
            $http.get('/refresh/activities/count/')
                .then(
                    function (data) {
                        $scope.update_count = 'Updated ' + data.data['count'] + ' records.';
                        $scope.error_messages = data.data['error_messages'];
                        console.log(data.data);
                    },
                    function (error) {
                        console.log(error);
                    }
                );
        };
    });

// myApp.config(['$routeProvider',
//      function($routeProvider) {
//          $routeProvider.
//              when('/', {
//                  templateUrl: '/static/partials/index.html'
//              }).
//              when('/about', {
//                  templateUrl: '../static/partials/about.html'
//              }).
//              otherwise({
//                  redirectTo: '/'
//              });
//     }]);

// })(window.angular);